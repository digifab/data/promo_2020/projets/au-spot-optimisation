#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 15 09:55:18 2020

@author: dev
"""

# LIBRAIRIES EXTERNES

from flask import Flask , render_template, redirect, url_for, flash, session, request, Response
from flask_login import (LoginManager, UserMixin,
                         login_required, login_user, logout_user,
                         current_user)
import datetime as date
import time
import pandas as pd
import sqlite3 as lite
from werkzeug.security import generate_password_hash, check_password_hash
import psycopg2

# IMPORTS LOCAUX
import auspot_graphs_analyses as as_analyses
import auspot_donnees_fictives as as_data

#BASE DE DONNEES FICTIVE
ADRESSE_BASE_DE_DONNEES = "base_fictive.db"

#BASE DE DONNEES "DIGIFAB" host DIGIFAB 192.168.1.79
# con = psycopg2.connect(host="192.168.1.79",
#                     database="digifab",
#                     user="postgres",
#                     password="digifab",
#                     port=5432)

# cur = con.cursor()

# INITIALISATION DE L'APPLICATION AVEC FLASK
app=Flask(__name__)
app.config.update(
    DEBUG = False,
    SECRET_KEY = 'secret_xxx'
)

login_manager = LoginManager()
login_manager.login_view = "login"
login_manager.init_app(app)

### FONCTIONS ET CONSTANTES NÉCESSAIRES
date_debut = date.datetime(2018, 1, 1)
date_fin = date.datetime(2020, 12, 31)
# as_data.generation_data_corr(date_debut, date_fin)
ventes_produits = as_data.generation_ventes_opti(date_debut, date_fin)



@app.route('/')
def info():
    title = "Accueil"
    return render_template("index.html", title=title)


@app.route('/form_previsions', methods=['GET', 'POST'])
def form_previsions():
    title = "Prévision"
    categories = list(set(ventes_produits.catégorie))
    categories.append('GÉNÉRAL')

    return render_template("form_prevision.html", title=title,
                           categories=categories)



@app.route('/previsions', methods=['GET', 'POST'])
def previsions():
    title = "Prévisions des commandes"
    imgs_right = []
    imgs_left = []
    categories = list(set(ventes_produits.catégorie))
    categories.append('GÉNÉRAL')

    montant = int(request.form['montant'])
    categorie = request.form['categorie']
    date_debut = date.datetime.strptime(request.form['date_debut'],
                                        "%Y-%m-%d")
    date_fin = date.datetime.strptime(request.form['date_fin'],
                                      "%Y-%m-%d")

    if categorie != 'GÉNÉRAL':
        mask = (ventes_produits['catégorie'] == categorie)
        donnees_ventes = ventes_produits[mask]
    else :
        donnees_ventes = ventes_produits

    imgs_right += ['images/graph_frontiere.png']

    imgs_left += ['images/repartition.png']
    w, produits, donnees_ventes = as_analyses.graph_frontiere_efficiente(
        date_debut, date_fin,
        donnees_ventes, imgs_right[0])
    data = as_analyses.graph_repartition_stocks(w, produits, donnees_ventes,
                                                montant, imgs_left[0])
    # gérer l'affichage du tableau avec un dico (voir param à passer)
    # data = data.to_dict()


    return render_template("prevision.html", data=data, imgs_right=imgs_right,
                           imgs_left=imgs_left, time=int(time.time()), 
                           categories=categories)


@app.route('/form_analyse', methods=['GET'])
def form_analyse():
    title = "Graphiques"
    cibles = list(ventes_produits.columns)
    modes = ['mois', 'annee']
    categories = list(set(ventes_produits.catégorie))
    categories.append('GÉNÉRAL')

    return render_template("form_analyse.html", title=title, cibles=cibles,
                           modes=modes, categories=categories)


@app.route('/analyse', methods=['GET'])
def analyse():
    """
   
    """
    cibles = list(ventes_produits.columns)
    modes = ['mois', 'annee']
    categories = list(set(ventes_produits.catégorie))
    categories.append('GÉNÉRAL')
    
    title = "Analyse financière"
    mode = request.args.get('mode')
    cible = request.args.get('cible')
    categorie = request.args.get('categorie')
    
    imgs_left = []
    imgs_right = []

    if categorie != 'GÉNÉRAL':
        mask = (ventes_produits['catégorie'] == categorie)
        donnees_ventes = ventes_produits[mask]
    else :
        donnees_ventes = ventes_produits


    imgs_left += ['images/correlation.png']
    as_analyses.graph_corr(donnees_ventes, imgs_left[0])

    imgs_right += ['images/violinplot.png']
    as_analyses.violinplot_analyse(donnees_ventes, mode, cible, imgs_right[0])

    imgs_right += ['images/lineplot.png']
    as_analyses.lineplot_analyse(donnees_ventes, mode, cible, imgs_right[1])

    return render_template("analyse.html", title=title, imgs_left=imgs_left,
                           imgs_right=imgs_right, time=int(time.time()), 
                           cibles=cibles, modes=modes, categories=categories)

@app.route('/creation_compte')
def creation_compte():

    """Ici nous envoyons une template qui permet la creation d'un compte.
       les informations nécéssaires sont les suivante : un nom de compte
       une addresse mail, et le mot de passe désiré en double"""

    if 'nom_de_compte' in session :
        return redirect("/")
    return render_template("formulaire_creation_compte.html")


@app.route('/traitement_creation_compte', methods=['POST'])
def traitement_creation_compte() :
    """Nous traiton le contenu du formulaire obtenu de la page de création
       Il est a noté que par sécurité le mot de passe de l'utilisateur ne doit
       JAMAIS être stocké sur la BDD en clair, et de limité au maximum les
       sotckage en variable en clair, si la requête est fait en HTTPS le mdp
       sera stocké au sein du formulaire encodé et donc ne sera jamais present
       sur le site sous quelque forme que ce soit en clair"""

    #Recuperation données formulaire
    nom_de_compte = request.form['nom_de_compte']
    mail = request.form['mail']

    #Verification que les mdp sont les même
    if request.form['mot_de_passe_1'] != request.form['mot_de_passe_2'] :
        return redirect("erreur_mdp")

    try :

        #Si non sallage du mot de passe avec werkzeug security
        mot_de_passe_sale = generate_password_hash(
            request.form['mot_de_passe_1'])

        today = datetime.datetime.today()

        modelisation.Utilisateur.create(mail = mail,
                                        pseudo = nom_de_compte,
                                        date_crea = today)

        var = modelisation.chargement_utilisateur(nom_de_compte)

        con = psycopg2.connect(host="192.168.1.79",
                               database="digifab",
                               user="postgres",
                               password="digifab",
                               port=5432)

        cur = con.cursor()
        requete = "INSERT INTO mot_de_passe VALUES ('"+ str(var) + "','"
        requete += str(mot_de_passe_sale) +"')"
        cur.execute(requete)
        con.commit()
        cur.close()
        login_user(var)
        #Comunique a l'utilisateur le succès
        return redirect('/creation_success')
    except :

        #Si le compte existe deja redirige vers une page qui indique l'echec
        #de sa tentative de creation de compte
        return redirect('/creation_fail')


@app.route('/creation_fail')
def creation_fail() :
    return render_template('message_creation_fail.html')


@app.route('/creation_success')
def creation_success() :
    return render_template('message_creation_success.html')


@app.route('/formulaire_authentification')
def authentification():
    if 'nom_de_compte' in session :
        return redirect("/")
    "Le formulaire de connexion se contente de récupéré l'adresse et le mdp"
    return render_template("formulaire_authentification.html")


@app.route('/traitement_authentification', methods=['GET','POST'])
def login():



    if request.method == 'POST':
        username = request.form['nom']

        con = psycopg2.connect(host="192.168.1.79",
                               database="digifab",
                               user="postgres",
                               password="digifab",
                               port=5432)

        cur = con.cursor()

        sql=f""" select * from utilisateur where "pseudo" = '{username}' """

        df = pd.read_sql_query(sql, con)
        id_utilisateur = df['id_utilisateur'][0]

        sql=f""" select * from mot_de_passe where "id_utilisateur" = '{id_utilisateur}' """

        df = pd.read_sql_query(sql, con)

        if check_password_hash(df['mot_de_passe'][0],
                               request.form['mot_de_passe']) :



            #id = df['user_id'][0]
            user = modelisation.chargement_utilisateur(username)
            pseudo_class = [user.pseudo]
            id_class = [user.id_utilisateur]
            login_user(user)
            con.close()

            return render_template('index.html')
        else:
            return ('zut')

    else:
        return render_template("formulaire_authentification.html")


@app.route('/test')
def test() :
    print("-"*10)
    print(ventes)
    type_graph = request.args.get('type')
    mode = request.args.get('mode')
    cible = request.args.get('cible')

    img = 'images/lineplot.png'
    as_analyses.lineplot_analyse(ventes, mode, cible, img)
    print(type_graph, mode, cible, img)

    print("-"*10)
    return redirect("/")


@app.route('/connexion_success')
def connexion_success() :
    return render_template('message_connexion_success.html')


@app.route('/connexion_fail')
def connexion_fail() :
    return render_template('message_connexion_fail.html')


@app.route('/deconnexion')
def deconnexion():

    return render_template('andy_page_deconnexion.html')


@app.route("/logout")
@login_required
def logout():
    logout_user()
    return Response('<p>Logged out</p>')


@login_manager.user_loader
def load_user(userid):
    try:
        test = modelisation.chargement_utilisateur(id_utilisateur = userid)

        class User(UserMixin):

            def __init__(self, id):
                self.id = id
                self.name = test.pseudo
                self.password = self.name + "_secret"
                self.utilisateur = test


            def __repr__(self):
                return "%d/%s/%s" % (self.id, self.name, self.password)
        return User(userid)

    except:
        print("Raté")
        return None

if __name__ == '__main__':
    app.run(debug=True, use_reloader = True)
